import superAgent from 'bodhi-driver-superagent';
import fs from 'fs';
import _ from 'lodash';
import microsJSON from './microstransdtl.json';

const Client = superAgent.Client;
const Basic = superAgent.BasicCredential;

const typeToCopy = 'MICROS_trans_dtl';

const client = new Client({
  uri : 'https://api.bodhi-qa.io',
  namespace : 'batman_bdi306',
  credentials : new Basic('admin__batman_bdi306', 'admin__batman_bdi306'),
  timeout: 60000
});

// const postPromise = promisize(postToClient.post);


const changedRecs = microsJSON.map(r => {
  const externalIds = r.external_ids;

  const nonStoreIdKeys = _.filter(externalIds, ({ key, value }) => {
    return key !== 'store_id';
  });

  nonStoreIdKeys.push({
    key: 'store_id',
    value: '5744b7560e26df5e368671b0'
  });

  r.external_ids = nonStoreIdKeys;
  delete r.sys_version;
  delete r.sys_created_at;
  delete r.sys_created_by;
  delete r.sys_type_version;
  delete r.sys_id;

  return r;
});

client.post('resources/MICROS_trans_dtl', JSON.stringify(changedRecs), (err, data) => {
  if(err) {
    console.log('err!', err.issues) 
  }

  console.log('DONE');
});

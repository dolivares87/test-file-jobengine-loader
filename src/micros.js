import _ from 'lodash';
import promisize from './lib/promisize';
import bodhiSuperAgent from 'bodhi-driver-superagent';

const Client = bodhiSuperAgent.Client;
const Basic = bodhiSuperAgent.BasicCredential;
const fromUrl = 'https://api.bodhi-qa.io';
const resource = 'MICROS_File';
const toStoreId = '570ea1d80e26df413c1c2235';


const connection = new Client({
  uri: fromUrl,
  namespace: 'micros_auto_2',
  credentials: new Basic('admin__micros_auto_2', 'admin__micros_auto_2'),
  timeout: 60000
});

const toConnection = new Client({
  uri: fromUrl,
  namespace: 'test_bdi_113_micros',
  credentials: new Basic('admin__test_bdi_113_micros', 'admin__test_bdi_113_micros'),
  timeout: 60000
});

const cloudLoader = (reqUrl, respHandler) => {
  connection.get(reqUrl, respHandler);
};

const cb = data => data;
const loadedPromise = promisize(cloudLoader);

const saveToNameSpace = (promises, payload) => {
  Promise.all(promises)
    .then(resp => {
      console.log('resp', resp);
      const upserter = (reqUrl, responseHandler, content) => {
        toConnection.post(reqUrl, content, responseHandler);
      };

      const upserterPromize = promisize(upserter);
      const loadedUpserts = [];

      const payLoadPromises = payload.forEach(load => {
        console.log('load', load.sysId);
        loadedUpserts.push(upserterPromize(`resources/MICROS_File`, null, load.type));
      });

      Promise.all(loadedUpserts)
        .then(r => {
          console.log('r', r);
        })
        .catch(err => console.log('err payLoadPromises', err));
    })
    .catch(err => console.log('err here', err));
};

const pumpToNamespace = (payload, originalData) => {
  const cloudFileUploader = (reqUrl, responseHandler, content) => {
    toConnection.file.uploadContent(reqUrl, content, responseHandler);
  };

  const uploaderPromize = promisize(cloudFileUploader);
  const loadedPayloads = [];

  payload.forEach(load => {
    loadedPayloads.push(uploaderPromize(load.type.content_url, null, load.payload));
  });

  saveToNameSpace(loadedPayloads, payload);
};

loadedPromise(`resources/MICROS_File`, cb).then(data => {
  const toS3Data = [];
  console.log('How much data', data.length);

  data.forEach(file => {
    const compData = file.local_file;

    toS3Data.push({
      payload: compData,
      sysId: file.sys_id,
      type: {
        local_file: '',
        store_id: file.store_id,
        local_name: file.local_name,
        compression: file.compression,
        processed: file.processed,
        content: file.content,
        group_type: file.group_type,
        DOB: '20160413',
        content_url: `store/${toStoreId}/micros/20160413/${file.local_name}`
      }
    });
  });

  pumpToNamespace(toS3Data, data);
})
.catch(err => console.log(err));

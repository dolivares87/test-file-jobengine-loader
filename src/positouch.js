import promisize from './lib/promisize';
import bodhiSuperAgent from 'bodhi-driver-superagent';

// Do from
const Client = bodhiSuperAgent.Client;
const Basic = bodhiSuperAgent.BasicCredential;

const connection = new Client({
  uri: 'https://api.bodhi-stg.io',
  namespace: 'bahrslanding',
  credentials: new Basic('admin__bahrslanding', 'admin__bahrslanding'),
  timeout: 60000
});

const toConnection = new Client({
  uri: 'https://api.bodhi-qa.io',
  namespace: 'feelthebern_bdi240',
  credentials: new Basic('admin__feelthebern_bdi240', 'admin__feelthebern_bdi240'),
  timeout: 60000
});

connection.get('resources/POSITOUCH_File', (err, data) => {
  if (err) {
    console.log('ERROR', err);
  }

  const newData = data.map(d => {
    return {
      count: d.count,
      local_file: d.local_file,
      store_id: '5720d57c0e26df4eafb55fc8',
      local_name: d.local_name,
      compression: d.compression,
      content_type: d.content_type,
      group_type: d.group_type,
      content_length: d.content_length,
      DOB: d.DOB,
      content_encoding: d.content_encoding,
      processed: false
    };
  });

  toConnection.post('resources/POSITOUCH_File', newData, (err, data) => {
    if (err) {
      console.log('ERROR post', err);
    }

    console.log('data', newData);
  });
});

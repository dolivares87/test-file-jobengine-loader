import superAgent from 'bodhi-driver-superagent';
import fileDownloader from 'bodhi-filedownloader';
import fs from 'fs';
import promisize from './lib/promisize';
import parseFile from 'parse-file';
import unzipContent from 'unzip-content';
import _ from 'lodash';

const Client = superAgent.Client;
const Basic = superAgent.BasicCredential;
const NAMESPACES_TO_SEARCH = [
  'jimmyhulas', 'theboynton', '13coinsrestaurants', '6smith', 
  '910westheimerlic', 'apostrophed', 'fivemonkeysllc', 'lakechalet', 'papanachos', 
  'theloopwestend'
];

function filesToProcessExist(adjTimeX, adjTime) {
  return adjTimeX.length === 0 && adjTime.length === 0;
}

function defaultHandler(referenceFile, downloadFile) {
  return Object.assign({}, referenceFile, { content: downloadFile.toString() });
}

function downloadFiles({ fileName, fileFilter, client }) {
  const loadedContents = fileDownloader({
    refResourceName: 'AlohaFile',
    urlPropPath: 'content_url',
    handler: defaultHandler,
    filter: fileFilter.replace('FILE_NAME', fileName),
    client
  });

  return loadedContents.then(files => Promise.resolve({ [fileName]: files }));
}

NAMESPACES_TO_SEARCH.forEach(async namespace => {
  const password = `admin__${namespace}`;
  const filter = "?where={DOB:'20160531',local_name:'FILE_NAME'}";

  const client = new Client({
    uri : 'https://api.bodhi.space',
    namespace : namespace,
    credentials : new Basic(password, password),
    timeout: 60000
  });

  const timeFiles = [
    downloadFiles({
      fileName: 'ADJTIMEX',
      fileFilter: filter,
      client
    }),
    downloadFiles({
      fileName: 'ADJTIME',
      fileFilter: filter,
      client
    })
  ];

  const contentFiles = await Promise.all(timeFiles);
  const alohaFiles = Object.assign({}, ...contentFiles);
  const adjTimeFiles = alohaFiles.ADJTIME || [];
  const adjTimexFiles = alohaFiles.ADJTIMEX || [];
  let filesToProcess;

  if (filesToProcessExist(adjTimexFiles, adjTimeFiles)) {
    filesToProcess = [];
  } else if (adjTimexFiles.length === 0) {
    filesToProcess = adjTimeFiles;
  } else {
    filesToProcess = _.reduce(adjTimexFiles, (files, adxFile) => {
      const dob = adxFile.DOB;
      const matchingAdjTimeFiles = _.filter(adjTimeFiles, f => (!f.DOB === dob));

      return [...files, ...matchingAdjTimeFiles];
    }, adjTimexFiles);
  }

  const namespacesWithNoClockOut = [];
  filesToProcess.forEach(refFile => {
    const { local_type: localType, content, content_encoding: contentEncoding } = refFile;
    const parsedTimecards = parseFile(localType, unzipContent(content, contentEncoding));

    parsedTimecards.forEach(tc => {
      const sysDateOut = tc.SYSDATEOUT;

      if (!sysDateOut) {
        namespacesWithNoClockOut.push({ 
          namespace,
          refFile
        });
      }
    })
  });

  if (namespacesWithNoClockOut.length) {
    const namespacesWithValidData = JSON.stringify(namespacesWithNoClockOut, null, 4) + '\n';
    const tcFilePath = `./${namespace}-opentimecards.json`;
    console.log('Writing timecards to file...', tcFilePath);
    try {
      
    fs.writeFileSync(tcFilePath, `${namespacesWithValidData}\n`);
    } catch (e) {
      console.log(e);
    }
  }
});

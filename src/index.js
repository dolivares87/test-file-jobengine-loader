import bdi from '../configs/brianbdi287.json';
import _ from 'lodash';
import promisize from './lib/promisize';
import bodhiSuperAgent from 'bodhi-driver-superagent';

const { files, transfer: { from, to } } = bdi;

// Do from
const Client = bodhiSuperAgent.Client;
const Basic = bodhiSuperAgent.BasicCredential;
const dobFilter = bdi.transfer.from.filter.DOB;
const mainFilter = `"DOB":"${dobFilter}"`;
console.log('mainfilter', mainFilter);

const dataFiles = fileName => _.includes(['JOB', 'EMP', 'REV', 'CAT', 'ITM'], fileName);
const url = from.env === 'prod' ? 'https://api.bodhi.space' : 'https://api.bodhi-stg.io';
const connection = new Client({
  uri: url,
  namespace: from.namespace,
  credentials: new Basic(from.username, from.password),
  timeout: 60000
});

const toUrl = to.env === 'prod' ? 'https://api.bodhi.space' : 'https://api.bodhi-qa.io';
const toConnection = new Client({
  uri: toUrl,
  namespace: to.namespace,
  credentials: new Basic(to.username, to.password),
  timeout: 60000
});

const resourceUrl = `resources/${from.fileResource}`;

const cloudLoader = (reqUrl, responseHandler) => {
  connection.get(reqUrl, responseHandler);
};

const dataHandler = incomingData => incomingData[0];
const loadedPromise = promisize(cloudLoader);
// ?where={"store_id":"561d363cda31494b345763f5", "DOB": "20160130"}
const promisizedFiles = files.map(file => {
  let additionalFilter;

  if (dataFiles(file)) {
    additionalFilter = '"DOB":"DATA"';
  } else {
    additionalFilter = mainFilter;
  }

  const completeUrl = `${resourceUrl}?where={"store_id":"${from.storeID}", ${additionalFilter},
            "${from.fileName}": "${file}"}`;

  return loadedPromise(completeUrl, dataHandler);
});

Promise.all(promisizedFiles)
.then(data => {
  const contentUrls = [];
  const filteredFile = data.filter(d => {
    return d ? true : false;
  });

  const mappedFilteredF = filteredFile.map(d => {
    const localName = d.local_name;
    let moddedContentUrl = '';

    if (dataFiles(localName)) {
      moddedContentUrl = `store/${to.storeID}/aloha/DATA/${d.local_name}`;
    } else {
      moddedContentUrl = `store/${to.storeID}/aloha/${bdi.transfer.from.filter.DOB}/${d.local_name}`;
    }

    if (d.content_url) {
      contentUrls.push({
        toUrl: moddedContentUrl,
        fromUrl: d.content_url
      });
    }

    if (!d.content_url && d.content) {
      if (dataFiles(localName)) {
        contentUrls.push({
          fromUrl: 'SPECIAL',
          toUrl: `store/${to.storeID}/aloha/DATA/${d.local_name}`,
          content: d.content
        });
      } else {
        contentUrls.push({
          fromUrl: 'SPECIAL',
          toUrl: `store/${to.storeID}/aloha/${bdi.transfer.from.filter.DOB}/${d.local_name}`,
          content: d.content
        });
      }
    }

    return {
      local_file: d.local_file,
      store_id: to.storeID,
      local_name: d.local_name,
      local_size: d.local_size,
      content_type: d.content_type,
      content: d.content,
      local_mtime: d.local_mtime,
      digest: d.digest,
      local_ctime: d.local_ctime,
      DOB: d.DOB,
      content_length: d.content_length,
      content_encoding: d.content_encoding,
      local_type: d.local_type,
      content_url: moddedContentUrl
    };
  });

  const cloudFileFetcher = (reqUrl, responseHandler) => {
    connection.file.download(reqUrl, responseHandler);
  };

  const fetcherPromize = promisize(cloudFileFetcher);
  // Copy files from source to qa
  const loadedFileFetchers = contentUrls.map(cnt => {
    const wrapper = fileData => {
      const contentToLoad = fileData ? fileData.toString('utf8') : cnt.content;
      return {
        toUrl: cnt.toUrl,
        content: contentToLoad
      };
    };

    return fetcherPromize(cnt.fromUrl, wrapper);
  });

  Promise.all(loadedFileFetchers)
    .then(storedData => {
      const cloudFileUploader = toConnection.file.uploadContent;
      const uploaderPromize = promisize(cloudFileUploader);

      const storedPromises = storedData.map(sData => {
        return uploaderPromize(sData.toUrl, null, sData.content);
      });

      // const onePr = storedPromises[0];
      Promise.all(storedPromises)
      .then(res => {
        const fileResourcePost = (reqUrl, content, responseHandler) => {
          toConnection.post(`resources/${reqUrl}`, content, responseHandler);
        };

        const postPromize = promisize(fileResourcePost);
        const promisesPost = mappedFilteredF.map(dataObj => {
          return postPromize(to.fileResource, null, dataObj);
        });

        Promise.all(promisesPost)
          .then(posted => console.log('posted', posted))
          .catch(err => console.log('promisespost', err));
        // console.log('res', res);
      })
      .catch(err => console.log('Post Error', err));
    })
    .catch(err => console.log('File fetchers', err));
})
.catch(err => console.log('Got the file content', err));

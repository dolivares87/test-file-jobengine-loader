import promisize from './lib/promisize';
import bodhiSuperAgent from 'bodhi-driver-superagent';

// Do from
const Client = bodhiSuperAgent.Client;
const Basic = bodhiSuperAgent.BasicCredential;

const connection = new Client({
  uri: 'https://api.bodhi.space',
  namespace: '10barrelbrewiingcompany',
  credentials: new Basic('admin__10barrelbrewiingcompany', 'admin__10barrelbrewiingcompany'),
  timeout: 60000
});

const toConnection = new Client({
  uri: 'https://api.bodhi-qa.io',
  namespace: 'feelthebern_allmicros',
  credentials: new Basic('admin__feelthebern_allmicros', 'admin__feelthebern_allmicros'),
  timeout: 60000
});

connection.get('resources/MICROS_File', (err, data) => {
  if (err) {
    console.log('ERROR', err);
  }

  const newData = data.map(d => {
    return {
      count: d.count,
      local_file: d.local_file,
      store_id: '5715070498d8cd4849f418d3',
      local_name: d.local_name,
      compression: d.compression,
      content_type: d.content_type,
      group_type: d.group_type,
      DOB: d.DOB,
      content_encoding: d.content_encoding,
      processed: false
    };
  });

  toConnection.post('resources/MICROS_File', newData, (err, data) => {
    if (err) {
      console.log('ERROR post', err);
    }

    console.log('data', newData);
  });
});

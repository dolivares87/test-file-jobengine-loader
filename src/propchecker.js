import superAgent from 'bodhi-driver-superagent';
import promisize from './lib/promisize';

const Client = superAgent.Client;
const Basic = superAgent.BasicCredential;
const client = new Client({
  uri : 'https://api.bodhi.space',
  namespace : 'fivemonkeysllc',
  credentials : new Basic('admin__fivemonkeysllc', 'admin__fivemonkeysllc'),
  timeout: 60000
});

const typeToCopy = 'HSTimecard';
const storeIdToCopyFrom = '563255fbda31491a977c4b5a';
const c = promisize(client.getAll);
const req = c(`resources/${typeToCopy}?where={store_id:'${storeIdToCopyFrom}'}`);



req.then(res => {
  console.log('got the files', res.length);
  res.forEach(tc => {
    if (!tc.employee_reference) {
      console.log('tc', tc);
    }
  });
}).catch(e => {
  console.log('error', e);
});

const promisize = requestHandler => (reqUrl, dataHandler, content) => {
  return new Promise((resolve, reject) => {
    const defaultResponseHandler = (err, data) => {
      if (err) {
        return reject(err);
      }

      if (data && data.length) {
        if (dataHandler) {
          resolve(dataHandler(data));
        } else {
          resolve(data);
        }
      } else {
        resolve(false);
      }
    };

    if (reqUrl === 'SPECIAL') {
      if (dataHandler) {
        resolve(dataHandler(content));
      } else {
        resolve(content);
      }
    }

    if (content) {
      console.log('requrl', reqUrl);
      requestHandler(reqUrl, content, defaultResponseHandler);
    } else {
      requestHandler(reqUrl, defaultResponseHandler);
    }
  });
};

export default promisize;

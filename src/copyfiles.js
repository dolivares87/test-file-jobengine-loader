import superAgent from 'bodhi-driver-superagent';
import fs from 'fs';
import promisize from './lib/promisize';
import _ from 'lodash';

const Client = superAgent.Client;
const Basic = superAgent.BasicCredential;
const client = new Client({
  uri : 'https://api.bodhi.space',
  namespace : 'gatorsdockside',
  credentials : new Basic('admin__gatorsdockside', 'admin__gatorsdockside'),
  timeout: 60000
});

const typeToCopy = 'MICROS_sale_dtl';
// const storeIdToCopyFrom = '563255fbda31491a977c4b5a';
const c = promisize(client.getAll);
const req = c(`resources/${typeToCopy}`);


const postToClient = new Client({
  uri : 'https://api.bodhi-qa.io',
  namespace : 'batman_bdi306',
  credentials : new Basic('admin__batman_bdi306', 'admin__batman_bdi306'),
  timeout: 60000
});

const postPromise = promisize(postToClient.post);

req.then(res => {
  console.log('got files!');
  const changedRecs = res.map(r => {
    const externalIds = r.external_ids;

    const nonStoreIdKeys = _.filter(externalIds, ({ key, value }) => {
      return key !== 'store_id';
    });

    nonStoreIdKeys.push({
      key: 'store_id',
      value: '5744b7560e26df5e368671b0'
    });

    r.external_ids = nonStoreIdKeys;
    delete r.sys_version;
    delete r.sys_created_at;
    delete r.sys_created_by;
    delete r.sys_type_version;
    delete r.sys_id;

    return r;
  });

  console.log('Got this many records', changedRecs.length);
  const jsonStr = JSON.stringify(changedRecs, null, 4) + '\n';
  // const reqUrl = `resources/${typeToCopy}`;

  fs.writeFileSync(`./${typeToCopy}.json`, jsonStr);
  console.log('Posting files to test store');

  // postPromise(reqUrl, null, JSON.stringify(changedRecs))
  //   .then(ress => {
  //     console.log('posted!', ress);
  //   })
  //   .catch(e => {
  //     console.log('errorincopy', e);
  //   });

}).catch(e => {
  console.log('error', e);
});

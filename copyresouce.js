import _ from 'lodash';
import bodhiSuperAgent from 'bodhi-driver-superagent';
import fs from 'fs';

// Do from
const Client = bodhiSuperAgent.Client;
const Basic = bodhiSuperAgent.BasicCredential;

const connection = new Client({
  uri: 'https://api.bodhi.space',
  namespace: 'fivemonkeysllc',
  credentials: new Basic('admin__fivemonkeysllc', 'admin__fivemonkeysllc'),
  timeout: 60000
});

const toConnection = new Client({
  uri: 'https://api.bodhi-qa.io',
  namespace: 'batman_bdi287_westside',
  credentials: new Basic('admin__batman_bdi287_westside', 'admin__batman_bdi287_westside'),
  timeout: 60000
});

toConnection.getAll('resources/HSSalesTransaction?where={business_day:"2016-05-03",store_id:"57423eea98d8cd482f246233"}', (err, data) => {
  if (err) {
    console.log('ERROR', err);
  }

  console.log('...got data');
  // const rejected = [];
  const added = [];
  const summation = data.reduce((sum, rec) => {
    if (rec.hasOwnProperty('guest_count')) {
      sum = sum + rec.guest_count;
      added.push(rec);
    } 

    return sum;
  }, 0);
  console.log('count', summation);
  const jsonStr = JSON.stringify(added, null, 4) + '\n';

  fs.writeFileSync('./wanted-westside.json', jsonStr);
  // const newData = data.map(rec => {
  //   const recCopy = _.cloneDeep(rec);
  //   const trimmedCP = _.omitBy(recCopy, (val, key) => /^(sys_*)/.test(key));
  //   trimmedCP.store_id = '5731e8b60e26df209dc78db4';
  //
  //   return trimmedCP;
  // });

  // console.log('...copying data');
  // toConnection.post('resources/HSSalesTransaction', newData, (err2, data2) => {
  //   if (err2) {
  //     console.log('ERROR post', err);
  //   }
  //
  //   console.log('data', data2);
  // });
});

import wantedJob from './wanted-job-westside.json';
import _ from 'lodash';
import wanted from './wanted-westside.json';

// const wantedJObj = JSON.parse(wantedJob);
// const wantedObj = JSON.parse(wanted);
const wantedJobRejected = [];
const wantedRejected = [];

function showTotal(files, from) {
  const summation = files.reduce((sum, rec) => {
    if (rec.hasOwnProperty('guest_count')) {
      sum = sum + rec.guest_count;
    } else {
      if (from === 'wanted') {
        wantedRejected.push(rec);
      } else {
        wantedJobRejected.push(rec);
      }
    } 

    return sum;
  }, 0);

  return summation;
}

let rejected;

const orderWanted = _.map(wanted, 'timestamp');
const orderJobWanted = _.map(wantedJob, 'timestamp');

console.log('orderwanted', orderWanted.length);
console.log('orderwantedjob', orderJobWanted.length);
const diffVals = [];
_.forEach(orderWanted, oW => {
  if (!orderJobWanted.includes(oW)) {
    diffVals.push(oW);
  }
})
const diff = _.difference(orderJobWanted, orderWanted);
const equalIs = _.isEqual(orderWanted, orderJobWanted);
console.log('diff', diff);
console.log('equal', equalIs);
console.log('sdf', typeof orderWanted[0]);

// const unionedJSON = _.unionWith(wanted, wantedJob, (wantedObjVal, wantedJObjVal) => {
//   return wantedObjVal.order_number !== wantedObjVal.order_number
// });

const wantedSum = showTotal(wanted, 'wanted');
const wantedJobSum = showTotal(wantedJob, 'wantedJob');
console.log('wantedlen', wanted.length);
console.log('wantedJoblen', wantedJob.length);
console.log('wantedSum', wantedSum);
console.log('wantedJobSum', wantedJobSum);
console.log('wantedRej', wantedRejected);
console.log('wantedJobRej', wantedJobRejected);
